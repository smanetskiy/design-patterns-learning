package com.manetskiy.patterns.structural.decorator;

// Component
public interface Car {
    void assemble();
}
