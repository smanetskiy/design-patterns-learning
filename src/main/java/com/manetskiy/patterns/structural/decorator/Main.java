package com.manetskiy.patterns.structural.decorator;

public class Main {
    public static void main(String[] args) {
        Car sportCar = new SportCar(new BasicCar());
        sportCar.assemble();

        System.out.println();

        Car luxurySportCar = new LuxuryCar(new SportCar(new BasicCar()));
        luxurySportCar.assemble();
    }
}
