package com.manetskiy.patterns.structural.decorator;

// Concrete Component
public class BasicCar implements Car {
    @Override
    public void assemble() {
        System.out.println("Basic car.");
    }
}
