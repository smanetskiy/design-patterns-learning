package com.manetskiy.patterns.structural.decorator;


// ConcreteDecoratorB
public class SportCar extends CarDecorator {
    public SportCar(Car car) {
        super(car);
    }

    public void assemble() {
        super.assemble();
        System.out.println("Adding sport car features.");
    }
}
