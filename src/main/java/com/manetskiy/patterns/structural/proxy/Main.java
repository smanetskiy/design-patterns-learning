package com.manetskiy.patterns.structural.proxy;

public class Main {
    public static void main(String[] args) {
        Internet internet = new ProxyInternet();
        internet.connectTo("ebook.com");
    }
}
