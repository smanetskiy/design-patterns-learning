package com.manetskiy.patterns.structural.proxy;

import java.util.ArrayList;
import java.util.List;

public class ProxyInternet implements Internet {
    private static final List<String> bannedSites;

    static {
        bannedSites = new ArrayList<>();
        bannedSites.add("xyz.com");
        bannedSites.add("pazuzu.edu");
        bannedSites.add("aldente.sp");
    }

    private Internet internet = new InternetImpl();


    @Override
    public void connectTo(String serverHost) {
        if (bannedSites.contains(serverHost)) {
            throw new IllegalArgumentException("Forbidden!");
        }

        internet.connectTo(serverHost);
    }
}
