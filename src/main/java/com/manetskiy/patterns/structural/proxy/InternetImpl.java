package com.manetskiy.patterns.structural.proxy;

public class InternetImpl implements Internet {
    @Override
    public void connectTo(String serverHost) {
        System.out.println("Connected to " + serverHost);
    }
}
