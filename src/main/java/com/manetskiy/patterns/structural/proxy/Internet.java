package com.manetskiy.patterns.structural.proxy;

public interface Internet {
    void connectTo(String serverHost);
}
