# Adapter Pattern:
* Makes a wrapper (Adapter) to create compatibility/conversion from one interface to the other interface which are incompatible.

* Wrapper (Adapter) works on two incompatible interfaces/classes.

* The intenstion of writing the wrapper class is to resolve the differences and make the interfaces compatible.

* We rarely add any functionality in the wrapper class. 

> https://dzone.com/articles/adapter-design-pattern-in-java 
