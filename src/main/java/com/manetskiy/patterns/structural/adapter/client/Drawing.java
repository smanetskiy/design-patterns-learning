package com.manetskiy.patterns.structural.adapter.client;

import com.manetskiy.patterns.structural.adapter.target.Shape;

import java.util.ArrayList;
import java.util.List;

public class Drawing {
    private List<Shape> shapes = new ArrayList<>();

    public void addShape(Shape shape) {
        shapes.add(shape);
    }

    public void draw() {
        if (shapes.isEmpty()) {
            System.out.println("Nothing to draw");
        }
        else {
            shapes.forEach(Shape::draw);
        }
    }

    public void resize() {
        if (shapes.isEmpty()) {
            System.out.println("Nothing to resize");
        }
        else {
            shapes.forEach(Shape::resize);
        }
    }
}
