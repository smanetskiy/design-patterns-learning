package com.manetskiy.patterns.structural.adapter.client;

import com.manetskiy.patterns.structural.adapter.adaptee.Rhombus;
import com.manetskiy.patterns.structural.adapter.adaptee.Triangle;
import com.manetskiy.patterns.structural.adapter.adapter.GeometricShapeObjectAdapter;
import com.manetskiy.patterns.structural.adapter.target.Circle;
import com.manetskiy.patterns.structural.adapter.target.Rectangle;

public class Main {
    public static void main(String[] args) {
        Drawing drawing = new Drawing();

        drawing.addShape(new Circle());
        drawing.addShape(new Rectangle());

        //Including adapted third party objects
        drawing.addShape(new GeometricShapeObjectAdapter(new Triangle(2, 2, 3)));
        drawing.addShape(new GeometricShapeObjectAdapter(new Rhombus()));

        drawing.draw();
        System.out.println("\n");
        drawing.resize();
    }
}
