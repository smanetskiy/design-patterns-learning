package com.manetskiy.patterns.structural.adapter.target;

public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("Drawing a Circle");
    }

    @Override
    public void resize() {
        System.out.println("Resizing a Circle");
    }

    @Override
    public String getDescription() {
        return "This is a Circle";
    }
}
