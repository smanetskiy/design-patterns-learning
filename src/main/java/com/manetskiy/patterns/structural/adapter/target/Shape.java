package com.manetskiy.patterns.structural.adapter.target;

// This is an interface that our client uses.
public interface Shape {
    void draw();
    void resize();
    String getDescription();
}
