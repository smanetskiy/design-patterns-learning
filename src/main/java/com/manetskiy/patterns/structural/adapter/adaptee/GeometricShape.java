package com.manetskiy.patterns.structural.adapter.adaptee;

//Imagine this is a third party interface that we need to adapt
public interface GeometricShape {
    double area();
    double perimeter();
    void drawShape();
}
