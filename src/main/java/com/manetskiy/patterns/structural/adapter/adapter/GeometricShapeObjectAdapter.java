package com.manetskiy.patterns.structural.adapter.adapter;

import com.manetskiy.patterns.structural.adapter.adaptee.GeometricShape;
import com.manetskiy.patterns.structural.adapter.adaptee.Rhombus;
import com.manetskiy.patterns.structural.adapter.adaptee.Triangle;
import com.manetskiy.patterns.structural.adapter.target.Shape;

public class GeometricShapeObjectAdapter implements Shape {
    private GeometricShape geometricShape;

    public GeometricShapeObjectAdapter(GeometricShape geometricShape) {
        this.geometricShape = geometricShape;
    }

    @Override
    public void draw() {
        geometricShape.drawShape();
    }

    @Override
    public void resize() {
        System.out.println(getDescription() + ". It cannot be resized. Please create a new one with required parameters!");
    }

    @Override
    public String getDescription() {
        if (geometricShape instanceof Triangle) {
            return "This is a Triangle";
        }
        else if (geometricShape instanceof Rhombus) {
            return "This is a Rhombus";
        }
        else{
            return "This is an Unknown shape.";
        }
    }
}
