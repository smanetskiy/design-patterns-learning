package com.manetskiy.patterns.structural.facade;

import com.manetskiy.patterns.structural.facade.subsystem.Engine;
import com.manetskiy.patterns.structural.facade.subsystem.FuelInjector;
import com.manetskiy.patterns.structural.facade.subsystem.Radiator;
import com.manetskiy.patterns.structural.facade.subsystem.Starter;

public class CarFacade {

    public void startCar() {
        FuelInjector.start();
        Starter.start();
        Radiator.start();
        Engine.start();
    }

    public void stopCar() {
        FuelInjector.stop();
        Starter.stop();
        Radiator.stop();
        Engine.stop();
    }
}
