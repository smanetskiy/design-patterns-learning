package com.manetskiy.patterns.structural.facade.subsystem;

public class Starter {
    public static void start() {
        System.out.println("Starter started.");
    }

    public static void stop() {
        System.out.println("Starter stopped.");
    }
}
