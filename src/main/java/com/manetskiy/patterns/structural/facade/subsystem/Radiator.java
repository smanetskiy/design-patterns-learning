package com.manetskiy.patterns.structural.facade.subsystem;

public class Radiator {
    public static void start() {
        System.out.println("Radiator started.");
    }

    public static void stop() {
        System.out.println("Radiator stopped.");
    }
}
