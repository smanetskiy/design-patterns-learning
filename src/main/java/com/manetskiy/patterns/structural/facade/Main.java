package com.manetskiy.patterns.structural.facade;

public class Main {
    public static void main(String[] args) {
        CarFacade facade = new CarFacade();

        // now we need 1 line of code to start or stop a car instead of invoking 4 methods each time.
        facade.startCar();

        facade.stopCar();
    }
}
