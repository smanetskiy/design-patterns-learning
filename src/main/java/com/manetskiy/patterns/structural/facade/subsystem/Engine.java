package com.manetskiy.patterns.structural.facade.subsystem;

public class Engine {
    public static void start() {
        System.out.println("Engine started.");
    }

    public static void stop() {
        System.out.println("Engine stopped.");
    }
}
