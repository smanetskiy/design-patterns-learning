package com.manetskiy.patterns.structural.facade.subsystem;

public class FuelInjector {
    public static void start() {
        System.out.println("FuelInjector started.");
    }

    public static void stop() {
        System.out.println("FuelInjector stopped.");
    }
}
