package com.manetskiy.patterns.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

// //The FlyweightFactory
public class FlavorsFactory {
    private static final Map<String, Coffee> flavorsCache = new HashMap<>();

    public static Coffee getCoffeeFlavor(String flavor) {
       return flavorsCache.computeIfAbsent(flavor, key -> {
           System.out.println("Adding " + flavor + " in cache");
           return new CoffeeImpl(flavor);
        });
    }

    public static int getFlavorsCount() {
        return flavorsCache.size();
    }
}
