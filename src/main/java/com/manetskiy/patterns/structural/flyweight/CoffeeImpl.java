package com.manetskiy.patterns.structural.flyweight;

public class CoffeeImpl implements Coffee {
    private final String flavor;

    public CoffeeImpl(String flavor) {
        this.flavor = flavor;
        System.out.println("Coffee was created with flavor: " + this.flavor);
    }

    public String getFlavor() {
        return flavor;
    }

    @Override
    public void serveCoffee(Table table) {
        System.out.println("Serving " + flavor + " at a table " + table.getTableNumber());
    }
}
