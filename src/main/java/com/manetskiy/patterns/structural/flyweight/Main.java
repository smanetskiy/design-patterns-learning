package com.manetskiy.patterns.structural.flyweight;

public class Main {
    public static void main(String[] args) {
        Coffee americano1 = FlavorsFactory.getCoffeeFlavor("Americano");
        Coffee americano2 = FlavorsFactory.getCoffeeFlavor("Americano");

        Coffee cappuccino1 = FlavorsFactory.getCoffeeFlavor("Cappuccino");
        Coffee cappuccino2 = FlavorsFactory.getCoffeeFlavor("Cappuccino");

        Table table1 = new Table(1);
        Table table2 = new Table(2);
        Table table3 = new Table(3);

        americano1.serveCoffee(table1);
        americano2.serveCoffee(table1);
        cappuccino1.serveCoffee(table2);
        cappuccino2.serveCoffee(table3);

        System.out.println();
        System.out.println("Number of Coffee instances that were created: " + FlavorsFactory.getFlavorsCount());
    }
}
