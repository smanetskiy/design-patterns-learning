package com.manetskiy.patterns.structural.flyweight;

public interface Coffee {
    void serveCoffee(Table context);
}
