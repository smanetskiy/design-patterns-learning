package com.manetskiy.patterns.structural.composite;

// Component
public interface Employee {
    void showEmployeeDetails();
}
