package com.manetskiy.patterns.structural.composite;

// Client
public class Main {
    public static void main(String[] args) {
        // Creating 1st composite
        CompanyDirectory devDirectory = new CompanyDirectory();
        devDirectory.addEmployee(new Developer(1, "Piotr", "Senior"));
        devDirectory.addEmployee(new Developer(2, "Rambo", "Junior"));

        // Creating 2nd composite
        CompanyDirectory managersDirectory = new CompanyDirectory();
        managersDirectory.addEmployee(new Manager(777, "Victor", "CEO"));
        managersDirectory.addEmployee(new Manager(898, "Angela", "Sales"));

        // Creating parent composite
        CompanyDirectory mainDirectory = new CompanyDirectory();
        mainDirectory.addEmployee(devDirectory);
        mainDirectory.addEmployee(managersDirectory);

        mainDirectory.showEmployeeDetails();
    }
}
