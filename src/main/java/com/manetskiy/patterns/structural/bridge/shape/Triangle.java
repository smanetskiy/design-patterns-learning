package com.manetskiy.patterns.structural.bridge.shape;

import com.manetskiy.patterns.structural.bridge.color.Color;

public class Triangle extends Shape {

    public Triangle(Color color) {
        super(color);
    }


    @Override
    public void applyColor() {
        System.out.print("Triangle filled with color: ");
        getColor().applyColor();
    }
}
