package com.manetskiy.patterns.structural.bridge.color;

public interface Color {
    void applyColor();
}
