package com.manetskiy.patterns.structural.bridge;

import com.manetskiy.patterns.structural.bridge.color.GreenColor;
import com.manetskiy.patterns.structural.bridge.color.RedColor;
import com.manetskiy.patterns.structural.bridge.shape.Pentagon;
import com.manetskiy.patterns.structural.bridge.shape.Shape;
import com.manetskiy.patterns.structural.bridge.shape.Triangle;

public class Main {
    public static void main(String[] args) {
        Shape triangle = new Triangle(new RedColor());
        Shape pentagon = new Pentagon(new GreenColor());

        triangle.applyColor();
        pentagon.applyColor();

        // Our code is not tightly coupled! We can set a different color.
        // We also can develop different color realisation without changing other code.
        // We can also add a new shape without changing color code.
        Shape anotherTriangle = new Triangle(new GreenColor());
        anotherTriangle.applyColor();
    }
}
