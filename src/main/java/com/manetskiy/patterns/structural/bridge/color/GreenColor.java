package com.manetskiy.patterns.structural.bridge.color;

public class GreenColor implements Color {
    @Override
    public void applyColor() {
        System.out.println("green.");
    }
}
