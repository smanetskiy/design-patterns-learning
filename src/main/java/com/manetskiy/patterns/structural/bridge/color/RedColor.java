package com.manetskiy.patterns.structural.bridge.color;

public class RedColor  implements Color {
    @Override
    public void applyColor() {
        System.out.println("red.");
    }
}
