package com.manetskiy.patterns.structural.bridge.shape;

import com.manetskiy.patterns.structural.bridge.color.Color;

public class Pentagon extends Shape {

    public Pentagon(Color color) {
        super(color);
    }

    @Override
    public void applyColor() {
        System.out.print("Pentagon filled with color: ");
        getColor().applyColor();
    }
}
