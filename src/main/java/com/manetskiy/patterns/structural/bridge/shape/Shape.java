package com.manetskiy.patterns.structural.bridge.shape;

import com.manetskiy.patterns.structural.bridge.color.Color;

public abstract class Shape {
    // Composition - Implementor
    private Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public abstract void applyColor();

    public Color getColor() {
        return color;
    }
}
