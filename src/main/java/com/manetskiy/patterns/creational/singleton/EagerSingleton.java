package com.manetskiy.patterns.creational.singleton;

public class EagerSingleton {
    private static final EagerSingleton EAGER_SINGLETON = new EagerSingleton();

    private EagerSingleton() {
        // do nothing
    }

    public static EagerSingleton getInstance() {
        return EAGER_SINGLETON;
    }
}
