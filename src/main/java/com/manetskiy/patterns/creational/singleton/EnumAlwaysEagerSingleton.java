package com.manetskiy.patterns.creational.singleton;


import com.manetskiy.patterns.creational.singleton.test.data.Connection;

public enum EnumAlwaysEagerSingleton {
    INSTANCE;

    private final Connection connection;

    EnumAlwaysEagerSingleton() {
        connection = new Connection("http://blabla.com");
        System.out.println("Created a new connection to " + connection.getUrl());
    }

    public Connection getConnection() {
        return connection;
    }
}
