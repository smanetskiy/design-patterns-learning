package com.manetskiy.patterns.creational.singleton.test.data;

public class Connection {
    private String url;

    public Connection(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
