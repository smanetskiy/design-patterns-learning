package com.manetskiy.patterns.creational.singleton;

public class DoubleLockLazySingleton {
    private static DoubleLockLazySingleton INSTANCE = null;

    private DoubleLockLazySingleton() {}

    public static DoubleLockLazySingleton getInstance() {
        if (INSTANCE == null) {
            synchronized (DoubleLockLazySingleton.class) {
                if (INSTANCE == null) {
                    synchronized (DoubleLockLazySingleton.class) {
                        INSTANCE = new DoubleLockLazySingleton();
                    }
                }
            }
        }
        return INSTANCE;
    }
}
