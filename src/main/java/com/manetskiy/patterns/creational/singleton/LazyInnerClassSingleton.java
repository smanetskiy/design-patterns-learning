package com.manetskiy.patterns.creational.singleton;

public class LazyInnerClassSingleton {

    private LazyInnerClassSingleton() {
    }

    private static class InstanceHolder {
        private static final LazyInnerClassSingleton INSTANCE = new LazyInnerClassSingleton();
    }

    public static LazyInnerClassSingleton getInstance() {
        return InstanceHolder.INSTANCE;
    }
}
