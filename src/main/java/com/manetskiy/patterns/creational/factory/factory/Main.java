package com.manetskiy.patterns.creational.factory.factory;

public class Main {
    public static void main(String[] args) {
        Computer pc = ComputerFactory.getComputer("PC", "Logitec 2GB", "Barracuda 2TB");
        pc.info();
    }
}
