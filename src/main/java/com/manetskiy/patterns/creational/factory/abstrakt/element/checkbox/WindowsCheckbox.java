package com.manetskiy.patterns.creational.factory.abstrakt.element.checkbox;


/**
 * Все семейства продуктов имеют одинаковые вариации (MacOS/Windows).
 *
 * Вариация чекбокса под Windows.
 */
public class WindowsCheckbox implements Checkbox {
    @Override
    public void paint() {
        System.out.println("You have created Windows checkbox!");
    }
}
