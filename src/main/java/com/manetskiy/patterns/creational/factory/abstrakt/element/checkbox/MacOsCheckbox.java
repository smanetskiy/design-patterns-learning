package com.manetskiy.patterns.creational.factory.abstrakt.element.checkbox;


/**
 * Все семейства продуктов имеют одинаковые вариации (MacOS/Windows).
 *
 * Вариация чекбокса под MacOS.
 */
public class MacOsCheckbox implements Checkbox {
    @Override
    public void paint() {
        System.out.println("You have created MacOS checkbox!");
    }
}
