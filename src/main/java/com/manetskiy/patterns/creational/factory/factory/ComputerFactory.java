package com.manetskiy.patterns.creational.factory.factory;

public class ComputerFactory {

    public static Computer getComputer(String type, String ram, String hdd) {
        if ("PC".equals(type)) {
            return new PC(ram, hdd);
        }
        if ("Server".equals(type)) {
            return new Server(ram, hdd);
        }

        throw new IllegalArgumentException("Invalid computer type");
    }
}
