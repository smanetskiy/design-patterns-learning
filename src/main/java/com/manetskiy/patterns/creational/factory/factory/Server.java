package com.manetskiy.patterns.creational.factory.factory;

public class Server extends Computer {

    public Server(String ram, String hdd) {
        super(ram, hdd);
    }

    @Override
    public void info() {
        System.out.printf("Type: Server with RAM: %s and HDD: %s", getRam(), getHdd());
    }
}
