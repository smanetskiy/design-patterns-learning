package com.manetskiy.patterns.creational.factory.factory;

public class PC extends Computer {

    public PC(String ram, String hdd) {
        super(ram, hdd);
    }

    @Override
    public void info() {
        System.out.printf("Type PC with RAM: %s and HDD: %s", getRam(), getHdd());
    }
}
