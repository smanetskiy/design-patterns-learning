package com.manetskiy.patterns.creational.factory.abstrakt.element.checkbox;

/**
 * Чекбоксы — это второе семейство продуктов. Оно имеет те же вариации, что и
 * кнопки.
 */
public interface Checkbox {
    void paint();
}
