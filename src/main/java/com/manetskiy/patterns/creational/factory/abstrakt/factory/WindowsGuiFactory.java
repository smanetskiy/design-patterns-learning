package com.manetskiy.patterns.creational.factory.abstrakt.factory;

import com.manetskiy.patterns.creational.factory.abstrakt.element.button.Button;
import com.manetskiy.patterns.creational.factory.abstrakt.element.button.WindowsButton;
import com.manetskiy.patterns.creational.factory.abstrakt.element.checkbox.Checkbox;
import com.manetskiy.patterns.creational.factory.abstrakt.element.checkbox.WindowsCheckbox;


/**
 * Каждая конкретная фабрика знает и создаёт только продукты своей вариации.
 */
public class WindowsGuiFactory implements GuiFactory {
    @Override
    public Button createButton() {
        return new WindowsButton();
    }

    @Override
    public Checkbox createCheckbox() {
        return new WindowsCheckbox();
    }
}
