package com.manetskiy.patterns.creational.factory.abstrakt;

import com.manetskiy.patterns.creational.factory.abstrakt.element.button.Button;
import com.manetskiy.patterns.creational.factory.abstrakt.element.checkbox.Checkbox;
import com.manetskiy.patterns.creational.factory.abstrakt.factory.GuiFactory;

public class App {
    private Button button;
    private Checkbox checkbox;

    public App(GuiFactory factory) {
        button = factory.createButton();
        checkbox = factory.createCheckbox();
    }

    public void paint() {
        button.paint();
        checkbox.paint();
    }
}
