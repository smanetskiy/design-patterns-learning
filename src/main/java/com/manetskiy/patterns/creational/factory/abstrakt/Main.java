package com.manetskiy.patterns.creational.factory.abstrakt;

import com.manetskiy.patterns.creational.factory.abstrakt.factory.GuiFactory;
import com.manetskiy.patterns.creational.factory.abstrakt.factory.MacOsGuiFactory;
import com.manetskiy.patterns.creational.factory.abstrakt.factory.WindowsGuiFactory;

public class Main {

    private static App createApp() {
        GuiFactory factory;
        String osName = System.getProperty("os.name");

        if (osName.contains("Mac")) {
            factory = new MacOsGuiFactory();
        }
        else {
            factory = new WindowsGuiFactory();
        }

        return new App(factory);
    }

    public static void main(String[] args) {
        App app = createApp();
        app.paint();
    }
}
