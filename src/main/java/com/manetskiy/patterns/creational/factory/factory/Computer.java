package com.manetskiy.patterns.creational.factory.factory;

public abstract class Computer {
    private String ram;
    private String hdd;

    public Computer(String ram, String hdd) {
        this.ram = ram;
        this.hdd = hdd;
    }

    public abstract void info();

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getHdd() {
        return hdd;
    }

    public void setHdd(String hdd) {
        this.hdd = hdd;
    }
}
