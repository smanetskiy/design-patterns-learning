package com.manetskiy.patterns.creational.factory.abstrakt.factory;

import com.manetskiy.patterns.creational.factory.abstrakt.element.button.Button;
import com.manetskiy.patterns.creational.factory.abstrakt.element.checkbox.Checkbox;


/**
 * Абстрактная фабрика знает обо всех (абстрактных) типах продуктов.
 */
public interface GuiFactory {
    Button createButton();
    Checkbox createCheckbox();
}
