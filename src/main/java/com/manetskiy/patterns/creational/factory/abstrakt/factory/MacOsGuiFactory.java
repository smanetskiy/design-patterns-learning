package com.manetskiy.patterns.creational.factory.abstrakt.factory;

import com.manetskiy.patterns.creational.factory.abstrakt.element.button.Button;
import com.manetskiy.patterns.creational.factory.abstrakt.element.button.MacOsButton;
import com.manetskiy.patterns.creational.factory.abstrakt.element.checkbox.Checkbox;
import com.manetskiy.patterns.creational.factory.abstrakt.element.checkbox.MacOsCheckbox;


/**
 * Каждая конкретная фабрика знает и создаёт только продукты своей вариации.
 */
public class MacOsGuiFactory implements GuiFactory {
    @Override
    public Button createButton() {
        return new MacOsButton();
    }

    @Override
    public Checkbox createCheckbox() {
        return new MacOsCheckbox();
    }
}
