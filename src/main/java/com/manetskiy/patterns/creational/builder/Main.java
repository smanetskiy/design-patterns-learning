package com.manetskiy.patterns.creational.builder;

public class Main {
    public static void main(String[] args) {
        BankAccount bankAccount = BankAccount.builder(99999991L)
                .balance(700_000L)
                .branch("Ohio")
                .interestRate(5000L)
                .owner("SM")
                .build();


        System.out.println(bankAccount);
    }
}
