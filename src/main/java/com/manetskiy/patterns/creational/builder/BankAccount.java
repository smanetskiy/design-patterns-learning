package com.manetskiy.patterns.creational.builder;

public class BankAccount {
    private long accountNumber; //This is important, so we'll pass it to the constructor.
    private String owner;
    private String branch;
    private double balance;
    private double interestRate;

    private BankAccount() {
        // Do nothing
    }

    public static Builder builder(long accountNumber) {
        return new Builder(accountNumber);
    }

    public static class Builder {
        private long accountNumber;
        private String owner;
        private String branch;
        private double balance;
        private double interestRate;

        private Builder() {
            // Do nothing
        }

        //accountNumber is mandatory
        public Builder(long accountNumber) {
            this.accountNumber = accountNumber;
        }

        public Builder owner(String owner) {
            this.owner = owner;
            return this;
        }

        public Builder branch(String branch) {
            this.branch = branch;
            return this;
        }

        public Builder balance(long balance) {
            this.balance = balance;
            return this;
        }

        public Builder interestRate(long interestRate) {
            this.interestRate = interestRate;
            return this;
        }

        public BankAccount build() {
            BankAccount bankAccount = new BankAccount();
            bankAccount.accountNumber = accountNumber;
            bankAccount.balance = balance;
            bankAccount.branch = branch;
            bankAccount.interestRate = interestRate;
            bankAccount.owner = owner;

            return bankAccount;
        }
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public String getOwner() {
        return owner;
    }

    public String getBranch() {
        return branch;
    }

    public double getBalance() {
        return balance;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public String toString() {
        return new StringBuilder()
                .append("BankAccount: \n")
                .append("accountNumber: " + accountNumber + "\n")
                .append("owner: " + owner + "\n")
                .append("branch: " + branch + "\n")
                .append("balance: " + balance + "\n")
                .append("interestRate: " + interestRate + "\n")
                .toString();
    }
}
