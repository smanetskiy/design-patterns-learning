package com.manetskiy.patterns.creational.prototype;

public class Leaf {

    private String form;

    public Leaf(String form) {
        this.form = form;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }
}
