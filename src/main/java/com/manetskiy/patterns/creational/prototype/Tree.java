package com.manetskiy.patterns.creational.prototype;

public abstract class Tree {
    private Integer height;
    private String treeName;
    private Leaf leaf;

    public Tree(Integer height, String treeName, Leaf leaf) {
        this.height = height;
        this.treeName = treeName;
        this.leaf = leaf;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getTreeName() {
        return treeName;
    }

    public void setTreeName(String treeName) {
        this.treeName = treeName;
    }

    public Leaf getLeaf() {
        return leaf;
    }

    public void setLeaf(Leaf leaf) {
        this.leaf = leaf;
    }

    public abstract Tree clone();
}
