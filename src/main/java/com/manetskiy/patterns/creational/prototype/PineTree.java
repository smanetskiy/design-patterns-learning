package com.manetskiy.patterns.creational.prototype;

public class PineTree extends Tree {

    public PineTree(Integer height, String treeName, Leaf leaf) {
        super(height, treeName, leaf);
    }

    public Tree clone() {
        return new PineTree(getHeight(), getTreeName(), new Leaf(this.getLeaf().getForm()));
    }
}
