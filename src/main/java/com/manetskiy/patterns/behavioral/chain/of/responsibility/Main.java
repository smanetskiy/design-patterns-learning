package com.manetskiy.patterns.behavioral.chain.of.responsibility;

public class Main {
    private static Server server;

    private static void init() {
        server = new Server();
        server.register("admin@example.com", "zaVorotnik");
        server.register("user@example.com", "user_pass");

        // Проверки связаны в одну цепь. Клиент может строить различные цепи,
        // используя одни и те же компоненты.
        Middleware middleware = new ThrottlingMiddleware(2);
        middleware.linkWith(new UserExistsMiddleware(server))
                .linkWith(new RoleCheckMiddleware());

        // Сервер получает цепочку от клиентского кода.
        server.setMiddleware(middleware);
    }

    public static void main(String[] args) {
        init();

        String email = "admin@example.com";
        String password = "zaVorotnik";

        server.logIn(email, password);

    }
}
