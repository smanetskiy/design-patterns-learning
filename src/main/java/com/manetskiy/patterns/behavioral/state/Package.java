package com.manetskiy.patterns.behavioral.state;

// Let's design our application. As already mentioned, the package can be ordered,
// delivered and received, therefore we're going to have three states and the context class.
public class Package {
    private PackageState packageState = new OrderedState();

    public void previousState() {
        packageState.previous(this);
    }

    public void nextState() {
        packageState.next(this);
    }

    public void printStatus() {
        packageState.printStatus();
    }

    public PackageState getPackageState() {
        return packageState;
    }

    public void setPackageState(PackageState packageState) {
        this.packageState = packageState;
    }
}
