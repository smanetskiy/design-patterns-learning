package com.manetskiy.patterns.behavioral.state;

// This interface will be implemented by each concrete state class.
public interface PackageState {
    void next(Package parcel);
    void previous(Package parcel);
    void printStatus();
}
