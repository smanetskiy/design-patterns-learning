package com.manetskiy.patterns.behavioral.state;

public class OrderedState implements PackageState {
    @Override
    public void next(Package parcel) {
        parcel.setPackageState(new DeliveredState());
    }

    @Override
    public void previous(Package parcel) {
        System.out.println("The package is in root state.");
    }

    @Override
    public void printStatus() {
        System.out.println("Ordered state.");
    }
}
