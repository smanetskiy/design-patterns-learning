package com.manetskiy.patterns.behavioral.state;

public class DeliveredState implements PackageState {
    @Override
    public void next(Package parcel) {
        parcel.setPackageState(new ReceivedState());
    }

    @Override
    public void previous(Package parcel) {
        parcel.setPackageState(new OrderedState());
    }

    @Override
    public void printStatus() {
        System.out.println("Delivered state");
    }
}
