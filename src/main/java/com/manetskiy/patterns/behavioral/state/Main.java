package com.manetskiy.patterns.behavioral.state;

public class Main {
    public static void main(String[] args) {
        Package pack = new Package();

        // ordered by client
        pack.printStatus();

        // delivered to post office
        pack.nextState();
        pack.printStatus();

        // received by client
        pack.nextState();
        pack.printStatus();

        // client wants to refund so he sent it back to post office
        pack.previousState();
        pack.printStatus();
    }
}
