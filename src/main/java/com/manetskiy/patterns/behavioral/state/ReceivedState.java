package com.manetskiy.patterns.behavioral.state;

public class ReceivedState implements PackageState {
    @Override
    public void next(Package parcel) {
        System.out.println("This package is already received by a client.");
    }

    @Override
    public void previous(Package parcel) {
        parcel.setPackageState(new DeliveredState());
    }

    @Override
    public void printStatus() {
        System.out.println("Received state.");
    }
}
