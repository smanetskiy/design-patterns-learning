package com.manetskiy.patterns.behavioral.visitor;

public class XmlElement extends Element {

    public XmlElement(String id) {
        super(id);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
