package com.manetskiy.patterns.behavioral.visitor;

public class JsonElement extends Element {

    public JsonElement(String id) {
        super(id);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
