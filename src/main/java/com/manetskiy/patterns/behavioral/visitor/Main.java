package com.manetskiy.patterns.behavioral.visitor;

public class Main {

    public static void main(String[] args) {
        Document rootDocument = new Document("rootId");

        rootDocument.addElement(new JsonElement("awesomeId"));
        rootDocument.addElement(new XmlElement("hyperAwesomeId"));

        Visitor visitor = new ElementVisitor();

        rootDocument.accept(visitor);
    }
}
