package com.manetskiy.patterns.behavioral.visitor;

public class ElementVisitor implements Visitor {

    @Override
    public void visit(JsonElement element) {
        System.out.println("Visited Json with id :: " + element.getId());
    }

    @Override
    public void visit(XmlElement element) {
        System.out.println("Visited Xml with id :: " + element.getId());
    }
}
