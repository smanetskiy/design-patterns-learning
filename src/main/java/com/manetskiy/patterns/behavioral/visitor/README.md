# Visitor

### Назначение
описывает операцию, выполняемую с каждым объектом из некоторой иерархии классов. Паттерн «Посетитель» позволяет определить новую операцию, не изменяя классов этих объектов.

### Когда использовать паттерн Посетитель
* Паттерн Посетитель определяет операцию, выполняемую на каждом элементе из некоторой структуры. Позволяет, не изменяя классы этих объектов, добавлять в них новые операции.
* Паттерн Посетитель позволяет выполнить нужные действия в зависимости от типов двух объектов.
* Предоставляет механизм двойной диспетчеризации.
