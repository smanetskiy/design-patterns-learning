package com.manetskiy.patterns.behavioral.visitor;

public abstract class Element {
    private final String id;

    public Element(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public abstract void accept(Visitor visitor);
}
