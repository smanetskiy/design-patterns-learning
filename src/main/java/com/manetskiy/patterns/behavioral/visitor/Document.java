package com.manetskiy.patterns.behavioral.visitor;

import java.util.ArrayList;
import java.util.List;

public class Document extends Element {
    private List<Element> elements = new ArrayList<>();

    public Document(String id) {
        super(id);
    }

    public void addElement(Element element) {
        elements.add(element);
    }

    @Override
    public void accept(Visitor visitor) {
        elements.forEach(element -> element.accept(visitor));
    }
}
