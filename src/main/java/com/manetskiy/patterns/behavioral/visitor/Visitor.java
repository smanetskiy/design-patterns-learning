package com.manetskiy.patterns.behavioral.visitor;

public interface Visitor {
    void visit(JsonElement element);
    void visit(XmlElement element);
}
