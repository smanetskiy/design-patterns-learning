package com.manetskiy.patterns.behavioral.observer;

public interface Unit {
    void update(Object object);
}
