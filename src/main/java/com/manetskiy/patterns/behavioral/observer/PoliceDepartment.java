package com.manetskiy.patterns.behavioral.observer;

import java.util.ArrayList;
import java.util.List;

public class PoliceDepartment {
    private String message;

    private List<Unit> units = new ArrayList<>();

    public void addUnit(Unit unit) {
        units.add(unit);
    }

    public void removeUnit(Unit unit) {
        units.remove(unit);
    }

    public void setMessage(String message) {
        this.message = message;
        units.forEach(unit -> unit.update(this.message));
    }
}
