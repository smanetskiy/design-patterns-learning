package com.manetskiy.patterns.behavioral.observer;

import java.util.Observer;

public class PoliceUnit implements Unit {
    private String unitName;
    private String message;

    public PoliceUnit(String unitName) {
        this.unitName = unitName;
    }

    @Override
    public void update(Object object) {
        message = (String) object;
        System.out.println("Unit " + unitName + " :: received message :: " + message);
    }
}
