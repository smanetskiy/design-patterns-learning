package com.manetskiy.patterns.behavioral.observer;

public class Main {
    public static void main(String[] args) {
        PoliceDepartment observable = new PoliceDepartment();

        PoliceUnit observer1 = new PoliceUnit("Patrol Car 12035");
        PoliceUnit observer2 = new PoliceUnit("Undercover Car 99021");

        observable.addUnit(observer1);
        observable.addUnit(observer2);

        observable.setMessage("A suspicious lama walks in the Main Road 42 area");
        observable.setMessage("The dog is barking in St. Peter's Boulevard");
    }
}
