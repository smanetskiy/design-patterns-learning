package com.manetskiy.patterns.behavioral.strategy;

public class Main {
    public static void main(String[] args) {
        BillingStrategy normalHours = BillingStrategy.normalStrategy();
        BillingStrategy happyHours = BillingStrategy.happyHoursStrategy();

        //normal hours
        Customer bob = new Customer(normalHours, "Bob");
        bob.add(100, 1);

        //happy hours
        bob.setBillingStrategy(happyHours);
        bob.add(100, 1);

        Customer paul = new Customer(happyHours, "Paul");
        paul.add(50, 1);
        paul.add(500, 1);

        //end of happy hours
        paul.setBillingStrategy(normalHours);
        paul.add(100, 1);

        bob.printBill(); //150
        paul.printBill(); //375

    }
}
