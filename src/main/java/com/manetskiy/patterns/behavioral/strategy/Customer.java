package com.manetskiy.patterns.behavioral.strategy;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private final List<Integer> drinks = new ArrayList<>();
    private final String name;
    private BillingStrategy billingStrategy;

    public Customer(BillingStrategy billingStrategy, String name) {
        this.billingStrategy = billingStrategy;
        this.name = name;
    }

    public void setBillingStrategy(BillingStrategy billingStrategy) {
        this.billingStrategy = billingStrategy;
    }

    public void add(int price, int quantity) {
        drinks.add(billingStrategy.getActualPrice(price * quantity));
    }

    public void printBill() {
        int bill = drinks.stream().mapToInt(price -> price).sum();
        System.out.println(name + " :: Total check: " + bill);
        drinks.clear();
    }
}
