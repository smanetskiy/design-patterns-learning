package com.manetskiy.patterns.behavioral.strategy;

@FunctionalInterface
public interface BillingStrategy {
    int getActualPrice(int price);

    static BillingStrategy normalStrategy() {
        return price -> price;
    }

    static BillingStrategy happyHoursStrategy() {
        return price -> price / 2;
    }
}
