package com.manetskiy.patterns.behavioral.memento;

public class Main {
    public static void main(String[] args) {
        TextEditor editor = new TextEditor();

        editor.write("This is a good day.\n");
        editor.write("I am a lazy lizard.\n");
        editor.hitSave();

        editor.write("This needs to be reverted.\n");

        System.out.println(editor.getText());

        editor.undo();
        System.out.println("-----------AFTER UNDO-------------");
        System.out.println(editor.getText());
    }
}
