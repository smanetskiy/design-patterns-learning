package com.manetskiy.patterns.behavioral.memento;

// Caretaker
public class TextEditor {
    private TextWindowState state;
    private TextWindow textWindow;

    public TextEditor() {
        this.textWindow = new TextWindow();
    }

    public void hitSave() {
        state = textWindow.saveText();
    }

    public void undo() {
        textWindow.restore(state);
    }

    public TextWindow getTextWindow() {
        return textWindow;
    }

    public void write(String text) {
        textWindow.addText(text);
    }

    public String getText() {
        return textWindow.getText();
    }

}
