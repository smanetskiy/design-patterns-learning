package com.manetskiy.patterns.behavioral.memento;

// Originator
public class TextWindow {
    private StringBuilder currentText;

    public TextWindow() {
        currentText = new StringBuilder();
    }

    public void addText(String text) {
        currentText.append(text);
    }

    public TextWindowState saveText() {
        return new TextWindowState(currentText.toString());
    }

    public void restore(TextWindowState state) {
        currentText = new StringBuilder(state.getText());
    }

    public String getText() {
        return currentText.toString();
    }
}
