package com.manetskiy.patterns.behavioral.interpreter;

import java.util.ArrayDeque;

public class Parser {
    public static Expr parseToken(String token, ArrayDeque<Expr> stack) {
        Expr right;
        // ...and then the left one
        Expr left;
        switch (token) {
            case "+":
                // It's necessary to remove first the right operand from the stack
                 right = stack.pop();
                // ...and then the left one
                 left = stack.pop();
                return Expr.plus(left, right);
            case "-":
                right = stack.pop();
                left = stack.pop();
                return Expr.minus(left, right);
            default:
                return Expr.variable(token);
        }
    }

    public static Expr parse(String expression) {
        ArrayDeque<Expr> stack = new ArrayDeque<>();
        for (String token : expression.split(" ")) {
            stack.push(parseToken(token, stack));
        }
        return stack.pop();
    }
}
