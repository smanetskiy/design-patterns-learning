package com.manetskiy.patterns.behavioral.interpreter;

import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Expr expr = Parser.parse("a b c + -");
        Map<String, Integer> context = Map.of("a", 12, "b", 6, "c", 36);

        int result = expr.interpret(context);

        System.out.println(result);

    }
}
