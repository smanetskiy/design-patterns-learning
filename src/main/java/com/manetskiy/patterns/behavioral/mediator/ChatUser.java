package com.manetskiy.patterns.behavioral.mediator;

public class ChatUser extends User {

    public ChatUser(ChatRoom chatRoom, String id, String name) {
        super(chatRoom, id, name);
    }

    @Override
    public void send(String message, String userId) {
        System.out.println(getName() + " :: is sending message :: " + message);
        getMediator().sendMessage(message, userId);
    }

    @Override
    public void receive(String message) {
        System.out.println(getName() + " :: has just received message :: " + message);
    }
}
