package com.manetskiy.patterns.behavioral.mediator;

public class Main {

    public static void main(String[] args) {
        // Mediator             Concrete Mediator
        ChatRoom chatRoom = new ChatRoomImpl();

        User user1 = new ChatUser(chatRoom, "aa", "Brian");
        User user2 = new ChatUser(chatRoom, "bb", "Dude");

        chatRoom.addUser(user1);
        chatRoom.addUser(user2);

        user1.send("Yo dude, wazzup", user2.getId());
        user2.send("Howdy!", user1.getId());
    }
}
