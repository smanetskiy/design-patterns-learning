package com.manetskiy.patterns.behavioral.mediator;

//Mediator
public interface ChatRoom {
    void sendMessage(String message, String userId);
    void addUser(User user);
}
