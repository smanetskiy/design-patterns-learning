package com.manetskiy.patterns.behavioral.mediator;

import java.util.HashMap;
import java.util.Map;

// Concrete Mediator
public class ChatRoomImpl implements ChatRoom {
    private final Map<String, User> usersByIds = new HashMap<>();

    @Override
    public void sendMessage(String message, String userId) {
        User user = usersByIds.get(userId);
        user.receive(message);
    }

    @Override
    public void addUser(User user) {
        usersByIds.put(user.getId(), user);
    }
}
