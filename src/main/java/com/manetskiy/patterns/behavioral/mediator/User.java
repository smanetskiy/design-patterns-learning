package com.manetskiy.patterns.behavioral.mediator;

public abstract class User {
    private ChatRoom mediator;

    private String id;
    private String name;

    public User(ChatRoom mediator, String id, String name) {
        this.mediator = mediator;
        this.id = id;
        this.name = name;
    }

    public abstract void send(String message, String userId);
    public abstract void receive(String message);

    public ChatRoom getMediator() {
        return mediator;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
