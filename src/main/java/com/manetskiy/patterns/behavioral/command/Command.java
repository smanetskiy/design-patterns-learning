package com.manetskiy.patterns.behavioral.command;

public interface Command {
    void execute();
}
