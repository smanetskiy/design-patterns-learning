package com.manetskiy.patterns.behavioral.command;

// Receiver
public class Light {
    private Boolean on;

    public void switchOn() {
        on = true;
        System.out.println("Light switched on.");
    }

    public void switchOff() {
        on = false;
        System.out.println("Light switched off.");
    }
}
