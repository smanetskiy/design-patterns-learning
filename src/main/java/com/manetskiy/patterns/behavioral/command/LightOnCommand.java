package com.manetskiy.patterns.behavioral.command;

public class LightOnCommand implements Command {
    // Reference to light
    private Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.switchOn();
    }
}
